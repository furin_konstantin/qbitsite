@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Редактирование тарифа @endslot
        @endcomponent
        
        <hr>

        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
        
        {{ Form::open(['route' => ['admin.price.update', $price], 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            {{ method_field('PUT') }}
            @include('admin.prices.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection