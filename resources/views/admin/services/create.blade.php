@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список услуг @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.service.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.services.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection