<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use App\Filters\QueryFilter;

class User extends Authenticatable
{
    use Notifiable;
    
    const ROLE_USER = 1;
    const ROLE_ADMIN = 10;
    
    protected $roles = [
        1 => 'Обычный пользователь',
        10 => 'Администратор'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function isAdmin() 
    {
        return $this->role === self::ROLE_ADMIN;
    }
    
    protected static function boot()
    {
        parent::boot();

        static::saving(function($user)
        {
            if (!$user->isAdmin()) {
                $user->role = User::ROLE_USER;
            }
        });
        static::saved(function($user)
        {
            
        });
        
        static::deleting(function($user)
        {
            if ($user->isAdmin()) {
                redirect()->route('admin.user_managment.user.index')->with('error', 'Нельзя удалять администраторов!');
                return false;
            }
        });
        static::deleting(function($user)
        {
            
        });
        
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
    public function strRole() {
        return $this->roles[$this->role];
    }
    
}
