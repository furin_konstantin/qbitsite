    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <div class="form-group">
        {{ Form::label('published', 'Статус') }}
        {{ Form::select('published', ['1' => 'Опубликовано', '0' => 'Не опубликовано'], $category->published ?? '', ['class' => 'form-control'])}}
    </div>
    
    <div class="form-group">
        {{ Form::label('title', 'Название') }}
        {{ Form::text('title', $category->title ?? '', ['class' => 'form-control', 'required', 'placeholder' => 'Заголовок категории']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('slug', 'Символьный код') }}
        {{ Form::text('slug', $category->slug ?? '', ['class' => 'form-control', 'placeholder' => 'Символьный код']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('parent_id', 'Родительская категория') }}
        <select class="form-control" name="parent_id">
            <option value="0">-- без родительской категории --</option>
            @include('admin.categories.partials.categories', ['categories' => $categories])
        </select>
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}