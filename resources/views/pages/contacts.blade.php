@extends('layouts.inner')

@section('title', 'Контакты')
@section('meta_keywords', 'Контакты')
@section('meta_description', 'Контакты')
@section('h1', 'Контакты')

@section('content')
    @parent
        @include('pages.partials.contacts.contants_block')
        <!-- Contact Form Section -->
        <section class="contact-form-section">
            <div class="auto-container">
                <div class="row clearfix">
                    @include('pages.partials.contacts.feedback_form')
                    @include('pages.partials.contacts.map')
                </div>
            </div>
        </section>
@endsection