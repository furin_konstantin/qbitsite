<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Advantage;
use App\Price;
use App\Service;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    
    public function dashboard() {
        return view('admin.dashboard', [
            'banners' => Banner::lastBanners(5),
            'advantages' => Advantage::lastAdvantages(5),
            'prices' => Price::lastPrices(5),
            'services' => Service::lastServices(5),
            'count_banners' => Banner::count(),
            'count_advantages' => Advantage::count(),
            'count_prices' => Price::count(),
            'count_services' => Service::count(),
        ]);
    }
    
}
