@extends('layouts.main')

@section('content')
    <!--Page Title-->
    <section class="page-title">
        <!-- Pattern Layers -->
        <div class="pattern-layers">
            <div class="layer-one" style="background-image: url('{{ asset(Config::get('constants.path_main_public_template').'/images/background/pattern-7.png')}}')"></div>
            <div class="layer-two" style="background-image: url('{{ asset(Config::get('constants.path_main_public_template').'/images/background/pattern-8.png')}}')"></div>
        </div>
        <div class="auto-container">
            <h2>@yield('h1')</h2>
            @component('components.breadcrumb', ['breadcrumbs' => $breadcrumbs])@endcomponent
        </div>
    </section>
    <!--End Page Title-->
@endsection