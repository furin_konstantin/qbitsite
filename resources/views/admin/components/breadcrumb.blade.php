<h2>{{$title}}</h2>
<ol class="breadcrumb">
    @foreach ($breadcrumbs as $breadcrumb)
        <li @if ($loop->last) class="active" @endif>@if (!$loop->last)<a href="{{ $breadcrumb['url'] ?? '' }}">@endif{{ $breadcrumb['title'] ?? '' }}@if (!$loop->last)</a>@endif</li>
    @endforeach
</ol>