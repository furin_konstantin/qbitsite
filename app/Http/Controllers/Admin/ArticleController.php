<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Filters\ArticleFilters;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ArticleFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Статьи'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'title',
                'label' => 'Наименование'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация'
            ]
        ];
        $arFilterFields = [
            [
                'name' => 'id',
                'label' => 'Идентификатор',
                'type' => 'text'
            ],
            [
                'name' => 'title',
                'label' => 'Поиск по названию',
                'type' => 'text'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация',
                'values' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано'
                ],
                'type' => 'select'
            ],
        ];
        return view('admin.articles.index', [
            'breadcrumbs' => $breadcrumbs,
            'filter_fields' => $arFilterFields,
            'sort_fields' => $arSortFields,
            'articles' => Article::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Статьи', 'url' => route('admin.article.index')],
            ['title' => 'Создание статьи']
        ];
        return view('admin.articles.create', [
            'breadcrumbs' => $breadcrumbs,
            'article' => [],
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => '',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'slug' => ['required', 'unique:articles'],
        ]);
        $data = $request->all();
        $model = new Article;
        if ($request->file('image')) {
            $data['image'] = AddFileInDB($request, $model, 'image');
        }
        $article = Article::create($data);
        // Save categories
        if ($request->input('categories')):
            $article->categories()->attach($request->input('categories'));
        endif;
        \Session::flash('success', 'Статья успешно добавлена!');
        
        return redirect()->route('admin.article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Статьи', 'url' => route('admin.article.index')],
            ['title' => $article->title]
        ];
        if (url()->getRequest()->delete_file && !empty($article->{url()->getRequest()->delete_file})) {
            if (\Storage::delete(str_replace('storage', 'public', $article->{url()->getRequest()->delete_file})) && \Storage::delete(str_replace('storage', \Config::get('constants.path_thumbnail'), $article->{url()->getRequest()->delete_file}))) {
                $article->{url()->getRequest()->delete_file} = "";
                $article->save();
                \Session::flash('success', 'Изображение успешно удалено!');
            }
        }
        return view('admin.articles.edit', [
            'breadcrumbs' => $breadcrumbs,
            'article' => $article,
            'categories' => Category::with('children')->where('parent_id', 0)->get(),
            'delimiter' => '',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $validator = $request->validate([
            'slug' => [
                'required', 
                \Illuminate\Validation\Rule::unique('articles')->ignore($article->id)
            ],
        ]);
        $data = $request->all();
        if (!isset($data['image_show'])) {
            $data['image_show'] = false;
        }
        if ($request->file('image')) {
            if ($article->image) {
                DeleteFileFromDB($article, 'image');
            }
            $data['image'] = AddFileInDB($request, $article, 'image');
        }

        $article->update($data);
        // Save categories
        // if ($request->input('categories')) {
            // $article->categories()->attach($request->input('categories'));
        // } else {
            // $article->categories()->detach();
        // }
        $article->categories()->sync($request->input('categories'));
        \Session::flash('success', 'Статья успешно обновлена!');
        
        return redirect()->route('admin.article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->categories()->detach();
        $article->delete();
        DeleteFileFromDB($article, 'image');
        
        \Session::flash('success', 'Статья успешно удалена!');
        
        return redirect()->route('admin.article.index');
    }
}
