<?php

namespace App;

use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BaseMenuTrait
 *
 * @package App
 */
trait BaseMenuTrait
{

    public function scopeIsActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeOfSort($query, $sort)
    {
        foreach ($sort as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }
    
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
    
    public function getParentsAttribute()
    {
        $res = collect([]);

        $parent = $this->parent;

        while(!is_null($parent)) {
            $res->push($parent);
            $parent = $parent->parent;
        }
        if (!$res->isEmpty()) {
            $res = $res->sort();
            return $res;
        }
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
    public function getOtherPartsUrl() {
        $res = "";
        if($this->parents) {
            $res = $this->parents->implode('url', '');
        }
        return $res;
    }
    
    public function getFullUrl() {
        $res = "";
        if (!$this->is_absolute_link) {
            $res = $this->getOtherPartsUrl() . $this->url;
        } else {
            $res = $this->url;
        }
        return $res;
    }
    
    public function isActiveItemMenu() {
        $res = false;
        $explodeUrl = explode('/', request()->path());
        $currentPath = '';
        foreach($explodeUrl as $explodePartUrl) {
            $currentPath .= '/' . $explodePartUrl;
            if ($currentPath == $this->getFullUrl()) {
                $res = true;
            }
        }
        return $res;
    }
    
    public function getElementsModel() {
        $res = [];
        $res = \Config::get('constants.list_models');
        array_unshift($res, 'Не выбрано');
        return $res;
    }
    
}