@extends('layouts.main')

@section('title', 'Q-bit')
@section('meta_keywords', 'Q-bit')
@section('meta_description', 'Q-bit')

@section('content')
    <!-- Main Slider -->
	<section class="main-slider">
		<!-- Pattern Layers -->
		<div class="pattern-layers">
			<div class="layer-one" style="background-image: url({{ asset(Config::get('constants.path_main_public_template').'images/main-slider/pattern-1.png')}})"></div>
			<div class="layer-two" style="background-image: url({{ asset(Config::get('constants.path_main_public_template').'images/main-slider/pattern-2.png')}})"></div>
		</div>
		<!-- Bottom Layers -->
		<div class="bottom-layers">
			<div class="layer-one"></div>
			<div class="layer-two"></div>
		</div>
        
        @if (count($banners) > 0)
            <div class="carousel-outer">
            
                <!-- Banner Carousel -->
                <div class="banner-carousel owl-theme owl-carousel">
                
                    <!-- Slide -->
                    @foreach($banners as $banner)
                        <div class="slide-item">
                            <div class="auto-container">
                                <div class="row clearfix">
                                    
                                    <!-- Content Column -->
                                    <div class="content-column col-lg-6 col-md-12 col-sm-12">
                                        <div class="inner-column">
                                            <div class="content">
                                                <h2>{{$banner->title}}</h2>
                                                <div class="text">{!! $banner->text !!}</div>
                                                @if($banner->button_title && $banner->link)
                                                    <div class="btns-box">
                                                        <a href="{{$banner->link}}" class="theme-btn btn-style-one"><span class="txt">{{$banner->button_title}}</span></a>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    
                                    @if ($banner->image)
                                        <!-- Image Column -->
                                        <div class="image-column col-lg-6 col-md-12 col-sm-12">
                                            <div class="inner-column">
                                                <div class="image">
                                                    <img src="{{asset($banner->getThumbImage())}}" alt="{{$banner->title}}" />
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    
                                </div>
                                
                            </div>
                        </div>
                    @endforeach
                    
                </div>
                
            </div>
        @endif
	</section>
	<!-- End Banner Section -->
	
	@if (count($advantages) > 0)
        <section class="offer-section">
            <div class="side-box"></div>
            <div class="auto-container">
                <div class="sec-title centered">
                    <h2>Преимущества</h2>
                </div>
                
                <div class="row clearfix">
                    
                    @foreach($advantages as $advantage)
                        <div class="offer-block col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <div class="icon-outer">
                                    <div class="icon-box">
                                        <span class="icon flaticon-{{$advantage->class}}"></span>
                                    </div>
                                </div>
                                <h3>{{$advantage->title}}</h3>
                                <div class="text">{!! $advantage->text !!}</div>
                            </div>
                        </div>     
                    @endforeach
                    
                </div>
                
            </div>
        </section>
    @endif
	
	<!-- Seo Section -->
	<section class="seo-section">
		<div class="auto-container">
			<div class="row clearfix">
				@include('pages.partials.index.content_1')
                @include('pages.partials.index.content_2')				
			</div>
		</div>
	</section>
	<!-- End Seo Section -->
	
	<!-- Services Section -->

    @if (count($services) > 0)
        <section class="services-section">
            <div class="auto-container">
                <div class="sec-title light centered">
                    <h2>Наши услуги</h2>
                </div>
                
                <div class="row clearfix">
                    
                    @component('widgets.services', ['services' => $services]) @endcomponent

                </div>
            </div>
        </section>
    @endif
	<!-- End Services Section -->
	
	<!-- Price Section -->
	<section class="price-section">
		<div class="side-box"></div>
		<div class="auto-container">
			<div class="row clearfix">
				
				<!-- Title Column -->
				<div class="title-column col-lg-4 col-md-12 col-sm-12">
					<div class="inner-column">
						
						<!-- Sec Title -->
                        @include('pages.partials.index.prices_desc')
					</div>
				</div>
				
				<!-- Blocks Column -->
                @if (count($prices) > 0)
                    <div class="blocks-column col-lg-8 col-md-12 col-sm-12">
                        <div class="inner-column">
                            <div class="row clearfix">
                                
                                <!-- Price Block -->
                                @foreach($prices as $price)
                                    <div class="price-block col-lg-6 col-md-6 col-sm-12">
                                        <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                            <div class="title">{{$price->title}}</div>
                                            <h2>{{$price->price}}</h2>
                                            <div class="text">{!! $price->description_short !!}</div>
                                            @if ($price->additional_text)
                                                {!! $price->additional_text !!}
                                            @endif
                                            <a href="{{route('price.detail', $price->slug)}}" class="theme-btn btn-style-two"><span class="txt">Подробнее</span></a>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                @endif
				
			</div>
		</div>
	</section>
@endsection