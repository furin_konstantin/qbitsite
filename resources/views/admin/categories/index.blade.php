@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список категорий @endslot
        @endcomponent
        
        <hr>

        <a href="{{route('admin.category.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"></i> Создать категорию</a>
        @include('admin.filter_form')
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
        <table class="table table-striped">
            <thead>
                @include('admin.sort')
                <th class="text-right">Действие</th>
            </thead>
            <tbody>
                @forelse ($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->title}}</td>
                        <td>@if($category->published)Да@elseНет@endif</td>
                        <td class="text-right">
                            <form onsubmit="if(confirm('Удалить?')){ return true; } else { return false; }" action="{{route('admin.category.destroy', $category)}}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <a href="{{route('admin.category.edit', ['category' => $category])}}"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="{{count($sort_fields)+1}}" class="text-center"><h2>Данные отсутствуют</h2></td>
                    </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="{{count($sort_fields)+1}}">
                        <ul class="pagination pull-right">
                            {{$categories->appends(url()->getRequest()->query())->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection