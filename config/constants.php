<?php

    return [
        'path_main_public_template' => 'site',
        'path_thumbnail' => 'public/thumbnail',
        'list_models' => [
            'App\Price' => 'Цены',
            'App\Service' => 'Услуги'
        ]
    ];