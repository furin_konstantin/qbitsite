@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список баннеров @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.banner.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.banners.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection