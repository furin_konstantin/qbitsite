@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список пунктов меню @endslot
        @endcomponent
        
        <hr>

        <a href="{{route('admin.menu.create', ['model' => $model])}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"></i> Создать пункт меню</a>
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
        <table class="table table-striped">
            <thead>
                @include('admin.sort')
                <th class="text-right">Действие</th>
            </thead>
            <tbody>
                @forelse ($menu_items as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->getFullUrl()}}</td>
                        <td>@if($item->active)Да@elseНет@endif</td>
                        <td>{{$item->sort_order}}</td>
                        <td>@if($item->parents){{ $item->parents->implode('name', ' / ') }}@endif</td>
                        <td class="text-right">
                            <form onsubmit="if(confirm('Удалить?')){ return true; } else { return false; }" action="{{route('admin.menu.destroy', ['item_id' => $item->id, 'model' => $model])}}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <a href="{{route('admin.menu.edit', ['model' => $model, 'item_id' => $item->id])}}"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="{{count($sort_fields)+1}}" class="text-center"><h2>Данные отсутствуют</h2></td>
                    </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="{{count($sort_fields)+1}}">
                        <ul class="pagination pull-right">
                            {{$menu_items->appends(url()->getRequest()->query())->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection