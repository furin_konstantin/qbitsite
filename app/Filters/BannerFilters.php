<?php

namespace App\Filters;

class BannerFilters extends QueryFilter
{

    public function id($id = "")
    {
        if ($id) {
            return $this->builder->where('id', $id);
        }
    }

    public function title($title = "")
    {
        if ($title) {
            return $this->builder->where('title', 'like', '%'.$title.'%');
        }
    }
    
    public function sort($sort)
    {
        if ($sort) {
            return $this->builder->orderBy($sort, $this->filters()["order"]);
        }
    }

    public function published($published = "")
    {
        if (is_numeric($published)) {
            return $this->builder->where('published', $published);
        }
    }
    
}