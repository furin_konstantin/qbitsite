<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 5)->create();
        $issetAdmin = User::where('email', 'admin@admin.ru')->first();
        if (!$issetAdmin) {
            DB::table('users')->insert([
                'name' => 'admin',
                'email' => 'admin@admin.ru',
                'email_verified_at' => now(),
                'password' => Hash::make('admin'),
                'remember_token' => Str::random(10),
                'role' => User::ROLE_ADMIN
            ]);
        }
    }
}
