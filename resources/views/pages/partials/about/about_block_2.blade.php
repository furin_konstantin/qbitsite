<!-- Video Column -->
<div class="video-column col-lg-6 col-md-12 col-sm-12">
    <div class="inner-column clearfix wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
        <div class="image">
            <img src="{{ asset(Config::get('constants.path_main_public_template').'/images/resource/seo.jpg')}}" alt="" />
        </div>
    <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="lightbox-image play-box"><span class="flaticon-play-button"><i class="ripple"></i></span></a>
    </div>
</div>