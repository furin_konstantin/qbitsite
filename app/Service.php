<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Filters\QueryFilter;

class Service extends Model
{

    protected $fillable = ['title', 'slug', 'description_short', 'description', 'image', 'meta_title', 'meta_description', 'meta_keyword', 'published', 'image_detail_1', 'image_detail_2', 'show_in_main', 'h1', 'sort'];
    
    public $pathImages = "public/services";
    public $pathThumbsImages = "public/thumbnail/services";
    public $pathStorageImages = "storage/services";
    public $pathStorageThumbImages = "storage/thumbnail/services";
    
    // Mutators
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug($value);
    }
    
    public function scopeLastServices($query, $count) {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
    
    public function getThumbImage($nameField) {
        $res = str_replace($this->pathStorageImages, $this->pathStorageThumbImages, $this->{$nameField});
        return $res;
    }
    
    public function scopeIndexServices($query, $count) {
        return $query->where('published', 1)->orderBy('sort', 'asc')->limit($count)->get();
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
}
