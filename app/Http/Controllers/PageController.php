<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Feedback;
use App\Banner;
use App\Advantage;
use App\Price;
use App\Service;


class PageController extends Controller
{

    public function index() {  
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Контакты'],
        ];
        
        $banners = Banner::where('published', 1)->orderBy('sort', 'asc')->get();
        $advantages = Advantage::where('published', 1)->orderBy('sort', 'asc')->get();
        $services = Service::where('show_in_main', 1)->indexServices(6);
        $prices = Price::where('published', 1)->where('show_in_main', 1)->orderBy('sort', 'asc')->limit(2)->get();
        
        return view('pages.index', [
            'breadcrumbs' => $breadcrumbs,
            'banners' => $banners,
            'advantages' => $advantages,
            'prices' => $prices,
            'services' => $services
        ]);
    }
    
    public function about() {  
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'О нас'],
        ];
        $services = Service::indexServices(3);
        $advantages = Advantage::where('published', 1)->orderBy('sort', 'asc')->get();
        return view('pages.about', [
            'breadcrumbs' => $breadcrumbs,
            'advantages' => $advantages,
            'services' => $services
        ]);
    }
    
    public function contacts() {
    
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Контакты'],
        ];
        
        return view('pages.contacts', [
            'breadcrumbs' => $breadcrumbs
        ]);
        
    }
    
    public function feedback(Request $request) {
        
        $resultSendMail = Mail::send(new Feedback($request->except('_token')));
        if (empty($resultSendMail)) {
            echo '<font color="green">С Вами свяжутся в ближайшее время!</font>';
        }
        
    }
    
}
