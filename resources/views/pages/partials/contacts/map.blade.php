<!-- Map Column -->
<div class="map-column col-lg-4 col-md-12 col-sm-12">
    <div class="inner-column">
        
        <div class="map-canvas"
            data-zoom="12"
            data-lat="-37.817085"
            data-lng="144.955631"
            data-type="roadmap"
            data-hue="#ffc400"
            data-title="Envato"
            data-icon-path="{{asset(Config::get('constants.path_main_public_template').'/images/icons/map-marker.png')}}"
            data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
        </div>
        
    </div>
</div>