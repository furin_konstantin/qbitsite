@extends('layouts.inner')

@section('title', 'Наши услуги')
@section('meta_keywords', 'Наши услуги')
@section('meta_description', 'Наши услуги')
@section('h1', 'Наши услуги')

@section('content')
    @parent
    @if (count($services) > 0)
        <section class="service-tabs-section style-two">
            <div class="auto-container">
                <div class="sec-title centered">
                    <h2>Наши услуги</h2>
                </div>
                
                <!-- Services Info Tabs -->
                <div class="services-info-tabs">
                    <!-- Service Tabs -->
                    <div class="services-tabs tabs-box">
                    
                        <!--Tab Btns-->
                        <ul class="tab-btns tab-buttons clearfix">
                            @foreach ($services as $service)
                                <li data-tab="#{{$service->slug}}" class="tab-btn @if ($loop->first) active-btn @endif"><span class="icon">@if($service->image)<img src="{{asset($service->getThumbImage('image'))}}" alt="{{$service->title}}" />@endif</span> {{$service->title}}</li>
                            @endforeach
                        </ul>
                        
                        <!--Tabs Container-->
                        <div class="tabs-content">
                            
                            <!--Tab / Active Tab-->
                            @foreach ($services as $service)
                                <div class="tab @if($loop->first) active-tab @endif" id="{{$service->slug}}">
                                    <div class="content">
                                        <div class="row clearfix">
                                            
                                            <!-- Content Column -->
                                            <div class="content-column col-lg-6 col-md-12 col-sm-12">
                                                <div class="inner-column">
                                                    <h2>{{$service->title}}</h2>
                                                    <div class="text">{{$service->description_short}}</div>
                                                    <a href="{{route('service.detail', $service->slug)}}" class="theme-btn btn-style-four">Узнать больше</a>
                                                </div>
                                            </div>
                                            
                                            <!-- Image Column -->
                                            @if ($service->image_detail_1 && $service->image_detail_2)
                                                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                                                    <div class="inner-column clearfix">
                                                        <div class="image image-one wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                                            <img src="{{asset($service->getThumbImage('image_detail_1'))}}" alt="{{$service->title}}" />
                                                        </div>
                                                        <div class="image image-two wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                                            <img src="{{asset($service->getThumbImage('image_detail_2'))}}" alt="{{$service->title}}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            @endforeach                            
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    @endif
@endsection