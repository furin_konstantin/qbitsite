$(function() {

    var options = {
        filebrowserBrowseUrl : '/admin/elfinder/ckeditor' 
    };

    if ($("#description_short").length) {
        CKEDITOR.replace( 'description_short', options );
    }
    if ($("#description").length) {
        CKEDITOR.replace( 'description', options );
    }
    if ($("#text").length) {
        CKEDITOR.replace( 'text', options );
    }
    if ($("#additional_text").length) {
        CKEDITOR.replace( 'additional_text', options );
    }
    
    $(document).on("click", ".js-sort", function() {
        window.location.href = $(this).data("sort_url");
    });
    
});