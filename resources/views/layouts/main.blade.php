<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title')</title>
<meta name="keywords" content="@yield('meta_keywords')">
<meta name="description" content="@yield('meta_description')">
<!-- Stylesheets -->
{!! Html::style(asset(Config::get('constants.path_main_public_template').'/css/app.css')) !!}
<link rel="shortcut icon" href="{{ asset(Config::get('constants.path_main_public_template').'/images/favicon.png')}}" type="image/x-icon">
<link rel="icon" href="{{ asset(Config::get('constants.path_main_public_template').'/images/favicon.png')}}" type="image/x-icon">

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="{{ asset(Config::get('constants.path_main_public_template').'/js/respond.js') }}"></script><![endif]-->
</head>

<body>

<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <header class="main-header header-style-one">
        
		<!-- Header Upper -->
        <div class="header-upper">
            <div class="inner-container">
                <div class="auto-container clearfix">
                    <!--Info-->
                    <div class="logo-outer">
                        <div class="logo"><a href="@if(request()->path() != '/'){{route('index')}}@else#@endif"><img src="{{asset(Config::get('constants.path_main_public_template').'/images/logo.png')}}" alt="" title=""></a></div>
                    </div>
                    @include('layouts.partials.header_menu')
                </div>
            </div>
        </div>
        <!--End Header Upper-->
		
		<!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel"></span></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="@if(request()->path() != '/'){{route('index')}}@else#@endif"><img src="{{asset(Config::get('constants.path_main_public_template').'/images/logo.png')}}" alt="" title=""></a></div>
                <div class="menu-outer">
					<ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
				</div>
            </nav>
        </div><!-- End Mobile Menu -->


    </header>
    <!-- End Main Header -->
    
    <!-- Content -->
        @yield('content')
    <!-- End Content -->
    
    <!-- Main Footer -->
    <footer class="main-footer">
		<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">
					<!--Footer Column-->
					<div class="footer-column col-lg-4 col-md-6 col-sm-12">
						<div class="footer-widget logo-widget">
							<div class="logo">
								<a href="@if(request()->path() != '/'){{route('index')}}@else#@endif"><img src="{{asset(Config::get('constants.path_main_public_template').'/images/footer-logo.png')}}" alt="" /></a>
							</div>
                            @include('layouts.partials.include.footer_text')
						</div>
					</div>
					
					@include('layouts.partials.footer_menu')
				</div>
			</div>
			
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="clearfix">
					<div class="pull-left">
						<div class="copyright">Amatic &copy; {{Carbon\Carbon::now()->format('Y')}} All Right Reserved</div>
					</div>
				</div>
			</div>
			
		</div>
	</footer>
	<!-- End Main Footer -->
	
</div>  
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-up"></span></div>

<!--Google Map APi Key-->
{!! Html::script('https://maps.googleapis.com/maps/api/js?key=AIzaSyBdGq4dBu5uddgrYCH_w0Ry7egwAJ4sEFA') !!}
{!! Html::script(asset(Config::get('constants.path_main_public_template').'/js/app.js')) !!}

</body>
</html>