@if ($breadcrumbs)
    <ul class="page-breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
            <li>@if (!$loop->last)<a href="{{ $breadcrumb['url'] ?? '' }}">@endif @if ($loop->first)<span class="icon fas fa-home"></span>@endif {{ $breadcrumb['title'] ?? '' }}@if (!$loop->last)</a>@endif</li>
        @endforeach
    </ul>
@endif