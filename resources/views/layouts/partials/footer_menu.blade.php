@if (count($footer_menu_items) > 0)
    <div class="footer-column col-lg-3 col-md-6 col-sm-12">
        <div class="footer-widget links-widget">
            <h2>Быстрые ссылки</h2>
            <div class="widget-content">
                <ul class="list">
                    @foreach($footer_menu_items as $footer_menu_item)
                        <li class="{{$footer_menu_item->isActiveItemMenu() ? 'current' : ''}}"><a href="{{$footer_menu_item->getFullUrl()}}">{{$footer_menu_item->name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif