@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Редактирование пунктов меню @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => ['admin.menu.update', 'item_id' => $menu_item->id, 'model' => $model], 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            {{ method_field('PUT') }}
            @include('admin.admin_menu.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection