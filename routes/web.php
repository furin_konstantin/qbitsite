<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'can:admin-panel']], function() {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/banner', 'BannerController', ['as' => 'admin']);
    Route::resource('/advantage', 'AdvantageController', ['as' => 'admin']);
    Route::resource('/price', 'PriceController', ['as' => 'admin']);
    Route::resource('/service', 'ServiceController', ['as' => 'admin']);
    Route::group(['prefix' => 'user_managment', 'namespace' => 'UserManagment'], function() {
        Route::resource('/user', 'UserController', ['as' => 'admin.user_managment']);
    });
    Route::group(['prefix' => 'menu', 'namespace' => 'AdminMenu'], function() {
        Route::get('/{model?}', 'AdminMenuController@index')->name('admin.menu.index');
        Route::post('/{model?}', 'AdminMenuController@store')->name('admin.menu.store');
        Route::get('/{model?}/create', 'AdminMenuController@create')->name('admin.menu.create');
        Route::put('/{model?}/{item_id?}', 'AdminMenuController@update')->name('admin.menu.update');
        Route::delete('/{model?}/{item_id?}', 'AdminMenuController@destroy')->name('admin.menu.destroy');
        Route::get('/{model?}/{item_id?}/edit', 'AdminMenuController@edit')->name('admin.menu.edit');
    });
});


Route::get('/', 'PageController@index')->name('index');
Route::get('/prices', 'PriceController@index')->name('price.index');
Route::get('/prices/{slug?}', 'PriceController@detail')->name('price.detail');
Route::get('/services', 'ServiceController@index')->name('service.index');
Route::get('/services/{slug?}', 'ServiceController@detail')->name('service.detail');
Route::get('/contacts', 'PageController@contacts')->name('contacts');
Route::post('/contacts', 'PageController@feedback')->name('contacts.feedback');
Route::get('/about', 'PageController@about')->name('about');

// Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
