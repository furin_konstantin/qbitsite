<?php

namespace App\Http\ViewComposers;

use App\HeaderMenu;
use App\FooterMenu;
use Illuminate\View\View;

class NavigationComposer
{
    public function compose(View $view)
    {
        $header_menu_items = $this->generateSimpleTree(HeaderMenu::class, false, ['parent_id' => 'asc', 'sort_order' => 'asc']);
        $footer_menu_items = $this->generateSimpleTree(FooterMenu::class, true, ['sort_order' => 'asc']);
        
        return $view->with('header_menu_items', $header_menu_items)->with('footer_menu_items', $footer_menu_items);
    }
    
    public function generateSimpleTree($className, $withoutParents = false, $arSort) {
        $res = $className::isActive()
            ->ofSort($arSort)
            ->get();
        if (!$withoutParents) {
            $res = $this->buildTree($res);
        }
        return $res;
    }
    
    public function buildTree($items)
    {
        $grouped = $items->groupBy('parent_id');
        foreach ($items as $item) {
            if ($grouped->has($item->id)) {
                $item->children = $grouped[$item->id];
            } elseif ($item->elements_model) {
                $model = new $item->elements_model;
                $modelList = $model->where('published', 1)->orderBy('sort')->get();
                $modelList->map(function ($model) {
                    $model->link = $this->setLinkForModel($model);
                });
                $item->modelChildren = $modelList;
            }
        }

        return $items->where('parent_id', null);
    }
    
    public function setLinkForModel($model)
    {
        $nameUrl = mb_strtolower(str_replace('App\\', '', get_class($model)));
        $res = route($nameUrl.'.detail', $model->slug);
        return $res;
    }
}