<?php

namespace App\Http\Controllers\Admin\UserManagment;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Filters\UserFilters;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UserFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Пользователи'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'name',
                'label' => 'Имя'
            ],
            [
                'name' => 'email',
                'label' => 'Email'
            ],
            [
                'name' => 'role',
                'label' => 'Роль'
            ]
        ];
        return view('admin.user_managment.users.index', [
            'breadcrumbs' => $breadcrumbs,
            'sort_fields' => $arSortFields,
            'users' => User::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Пользователи', 'url' => route('admin.user_managment.user.index')],
            ['title' => 'Создание пользователя']
        ];
        return view('admin.user_managment.users.create', [
            'breadcrumbs' => $breadcrumbs,
            'user' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password'])
        ]);
        \Session::flash('success', 'Пользователь успешно добавлен!');
        return redirect()->route('admin.user_managment.user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Пользователи', 'url' => route('admin.user_managment.user.index')],
            ['title' => $user->name]
        ];
        return view('admin.user_managment.users.edit', [
            'breadcrumbs' => $breadcrumbs,
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                \Illuminate\Validation\Rule::unique('users')->ignore($user->id)
            ],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
        ]);
        
        $user->name = $request['name'];
        $user->email = $request['email'];
        $request['password'] == null ?: $user->password = bcrypt($request['password']);
        $user->save();
        \Session::flash('success', 'Пользователь успешно обновлен!');
        return redirect()->route('admin.user_managment.user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        if (!$user->isAdmin()) {
            \Session::flash('success', 'Пользователь успешно удален!');
        }
        return redirect()->route('admin.user_managment.user.index');
    }

}
