@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список пользователей @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.user_managment.user.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.user_managment.users.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection