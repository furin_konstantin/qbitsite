@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="jumbotron">
                    <p><span class="label label-primary">Баннеров {{$count_banners ?? ''}}</span></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="jumbotron">
                    <p><span class="label label-primary">Преимуществ {{$count_advantages ?? ''}}</span></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="jumbotron">
                    <p><span class="label label-primary">Тарифов {{$count_prices ?? ''}}</span></p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="jumbotron">
                    <p><span class="label label-primary">Услуг {{$count_services ?? ''}}</span></p>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.banner.create')}}">Создать баннер</a>
                @foreach($banners as $banner)
                    <a class="list-group-item" href="{{route('admin.banner.edit', $banner)}}">
                        <h4 class="list-group-item-heading">{{$banner->title ?? ''}}</h4>
                    </a>
                @endforeach
            </div>
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.advantage.create')}}">Создать преимущество</a>
                @foreach($advantages as $advantage)
                    <a class="list-group-item" href="{{route('admin.advantage.edit', $advantage)}}">
                        <h4 class="list-group-item-heading">{{$advantage->title ?? ''}}</h4>
                    </a>
                @endforeach
            </div>
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.price.create')}}">Создать тариф</a>
                @foreach($prices as $price)
                    <a class="list-group-item" href="{{route('admin.price.edit', $price)}}">
                        <h4 class="list-group-item-heading">{{$price->title ?? ''}}</h4>
                    </a>
                @endforeach
            </div>
            <div class="col-sm-6">
                <a class="btn btn-block btn-default" href="{{route('admin.service.create')}}">Создать услугу</a>
                @foreach($services as $service)
                    <a class="list-group-item" href="{{route('admin.service.edit', $service)}}">
                        <h4 class="list-group-item-heading">{{$service->title ?? ''}}</h4>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection