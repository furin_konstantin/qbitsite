@extends('layouts.inner')

@section('title', $service['meta_title'])
@section('meta_keywords', $service['meta_keyword'])
@section('meta_description', $service['meta_description'])
@section('h1', $service['h1'])

@section('content')
    @parent
    <section class="seo-section-six">
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2>{{$service['title']}}</h2>
						<div class="text">
							{!! $service['description'] !!}
						</div>
					</div>
				</div>
				<!-- Images Column -->
                @if ($service->image_detail_1 && $service->image_detail_2)
                    <div class="image-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-column clearfix">
                            <div class="image image-one wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <img src="{{asset($service->getThumbImage('image_detail_1'))}}" alt="{{$service['title']}}" />
                            </div>
                            <div class="image image-two wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <img src="{{asset($service->getThumbImage('image_detail_2'))}}" alt="{{$service['title']}}" />
                            </div>
                        </div>
                    </div>
                @endif
			</div>
		</div>
	</section>
    <section class="services-page-section style-two">
		<div class="auto-container">
			<div class="sec-title centered">
				<h2>Наши услуги</h2>
			</div>
			
			<div class="row clearfix">
				
				@component('widgets.services', ['services' => $services]) @endcomponent

			</div>
		</div>
	</section>    
@endsection