@extends('layouts.inner')

@section('title', 'Тарифы')
@section('meta_keywords', 'Тарифы')
@section('meta_description', 'Тарифы')
@section('h1', 'Тарифы')

@section('content')
    @parent
    @if (count($prices) > 0)
        <section class="price-section alternate">
            <div class="auto-container">
                <div class="sec-title centered">
                    <h2>Список тарифиов</h2>
                </div>
                <div class="row clearfix">
                    <!-- Price Block Two -->
                    @foreach ($prices as $price)
                        <div class="price-block-two col-lg-4 col-md-6 col-sm-12">
                            <div class="inner-box">
                                <div class="title">{{$price->title}}</div>
                                <h2>{{$price->price}}</h2>
                                <div class="text">{!! $price->description_short !!}</div>
                                {!! $price->additional_text !!}
                                <a href="{{route('price.detail', $price->slug)}}" class="theme-btn btn-style-two"><span class="txt">Посмотреть тариф</span></a>
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </section>
    @endif
@endsection