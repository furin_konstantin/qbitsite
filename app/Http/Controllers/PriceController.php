<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;
use App\Service;

class PriceController extends Controller
{
    
    public function detail($slug) {
        $price = Price::where('slug', $slug)->first();
        if (!$price) {
            abort(404);
        }
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Тарифы', 'url' => route('price.index')],
            ['title' => $price['title']],
        ];
        $services = Service::indexServices(6);
        return view('prices.detail', [
            'breadcrumbs' => $breadcrumbs,
            'price' => $price,
            'services' => $services
        ]);
    }
    
    public function index() {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Тарифы'],
        ];
        $prices = Price::where('published', 1)->orderBy('created_at', 'desc')->get();
        return view('prices.index', [
            'breadcrumbs' => $breadcrumbs,
            'prices' => $prices
        ]);
    }
    
}
