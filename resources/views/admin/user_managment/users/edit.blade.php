@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Редактирование пользователя @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => ['admin.user_managment.user.update', $user], 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            {{ method_field('PUT') }}
            @include('admin.user_managment.users.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection