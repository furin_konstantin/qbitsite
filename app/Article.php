<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Filters\QueryFilter;

class Article extends Model
{

    protected $fillable = ['title', 'slug', 'description_short', 'description', 'image', 'image_show', 'meta_title', 'meta_description', 'meta_keyword', 'published', 'viewed', 'created_by', 'modified_by'];
    
    public $pathImages = "public/articles";
    public $pathThumbsImages = "public/thumbnail/articles";
    
    // Mutators
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug($value);
    }
    
    public function categories() {
        return $this->morphToMany('App\Category', 'categoryable');
    }
    
    public function scopeLastArticles($query, $count) {
        return $query->orderBy('created_by', 'desc')->take($count)->get();
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
}
