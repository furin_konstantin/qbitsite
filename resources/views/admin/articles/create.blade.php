@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список статей @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.article.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.articles.partials.form')
            {{ Form::hidden('created_by', Auth::id()) }}
        {{ Form::close() }}
        
    </div>
@endsection