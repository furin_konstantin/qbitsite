<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Price;
use Illuminate\Http\Request;
use App\Filters\PriceFilters;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PriceFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Тарифы'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'title',
                'label' => 'Наименование'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация'
            ]
        ];
        $arFilterFields = [
            [
                'name' => 'id',
                'label' => 'Идентификатор',
                'type' => 'text'
            ],
            [
                'name' => 'title',
                'label' => 'Поиск по названию',
                'type' => 'text'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация',
                'values' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано'
                ],
                'type' => 'select'
            ],
        ];
        return view('admin.prices.index', [
            'breadcrumbs' => $breadcrumbs,
            'filter_fields' => $arFilterFields,
            'sort_fields' => $arSortFields,
            'prices' => Price::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Тарифы', 'url' => route('admin.price.index')],
            ['title' => 'Создание тарифа']
        ];
        return view('admin.prices.create', [
            'breadcrumbs' => $breadcrumbs,
            'price' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'slug' => ['required', 'unique:prices'],
        ]);
        $data = $request->all();
        $model = new Price;
        if ($request->file('image')) {
            $data['image'] = AddFileInDB($request, $model, 'image', ['width' => 80, 'height' => 75]);
        }
        if ($request->file('image_detail_1')) {
            $data['image_detail_1'] = AddFileInDB($request, $model, 'image_detail_1', ['width' => 360, 'height' => 450]);
        }
        if ($request->file('image_detail_2')) {
            $data['image_detail_2'] = AddFileInDB($request, $model, 'image_detail_2', ['width' => 490, 'height' => 300]);
        }
        $price = Price::create($data);
        \Session::flash('success', 'Тариф успешно добавлен!');
        
        return redirect()->route('admin.price.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Тарифы', 'url' => route('admin.price.index')],
            ['title' => $price->title]
        ];
        if (url()->getRequest()->delete_file && !empty($price->{url()->getRequest()->delete_file})) {
            if (\Storage::delete(str_replace('storage', 'public', $price->{url()->getRequest()->delete_file})) && \Storage::delete(str_replace('storage', \Config::get('constants.path_thumbnail'), $price->{url()->getRequest()->delete_file}))) {
                $price->{url()->getRequest()->delete_file} = "";
                $price->save();
                \Session::flash('success', 'Изображение успешно удалено!');
            }
        }
        return view('admin.prices.edit', [
            'breadcrumbs' => $breadcrumbs,
            'price' => $price,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Price $price)
    {
        $validator = $request->validate([
            'slug' => [
                'required', 
                \Illuminate\Validation\Rule::unique('prices')->ignore($price->id)
            ],
        ]);
        $data = $request->all();
        if (!isset($data['show_in_main'])) {
            $data['show_in_main'] = false;
        }
        if ($request->file('image')) {
            if ($price->image) {
                DeleteFileFromDB($price, 'image');
            }
            $data['image'] = AddFileInDB($request, $price, 'image', ['width' => 80, 'height' => 75]);
        }

        if ($request->file('image_detail_1')) {
            if ($price->image) {
                DeleteFileFromDB($price, 'image_detail_1');
            }
            $data['image_detail_1'] = AddFileInDB($request, $price, 'image_detail_1', ['width' => 360, 'height' => 450]);
        }
        if ($request->file('image_detail_2')) {
            if ($price->image_detail_2) {
                DeleteFileFromDB($price, 'image_detail_2');
            }
            $data['image_detail_2'] = AddFileInDB($request, $price, 'image_detail_2', ['width' => 490, 'height' => 300]);
        }

        $price->update($data);
        \Session::flash('success', 'Тариф успешно обновлен!');
        
        return redirect()->route('admin.price.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
        $price->delete();
        DeleteFileFromDB($price, 'image');
        DeleteFileFromDB($price, 'image_detail_1');
        DeleteFileFromDB($price, 'image_detail_2');
        
        \Session::flash('success', 'Тариф успешно удален!');
        
        return redirect()->route('admin.price.index');
    }
}
