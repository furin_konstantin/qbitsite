@if ($filter_fields)
    <form method="get" action="{{url()->full()}}">
        @php
            $currentRequest = url()->getRequest()->toArray();
        @endphp
        @foreach ($filter_fields as $filter_field)
            @switch($filter_field['type'])
                @case('text')
                    <div class="form-group">
                        <label for="{{$filter_field['name']}}">{{$filter_field['label']}}</label>
                        <input type="text" name="{{$filter_field['name']}}" value="{{$currentRequest[$filter_field['name']]  ?? ''}}" class="form-control" id="{{$filter_field['label']}}" placeholder="{{$filter_field['label']}}">
                    </div>
                    @break
                @case('select')
                    <div class="form-group">
                        <label for="{{$filter_field['name']}}">{{$filter_field['label']}}</label>
                        <select name="{{$filter_field['name']}}" class="form-control" id="{{$filter_field['name']}}">
                            <option value="">Не выбрано</option>
                            @foreach ($filter_field['values'] as $key => $value)
                                <option @if(isset($currentRequest[$filter_field['name']]) && $currentRequest[$filter_field['name']] == $key) selected='selected' @endif value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    @break
            @endswitch
        @endforeach
        <a href="{{url()->current()}}" class="btn btn-primary">Сбросить</a>
        <button type="submit" class="btn btn-primary">Поиск</button>
    </form>
@endif