<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\NavigationComposer;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        view()->composer(['layouts.partials.header_menu', 'layouts.partials.footer_menu'], NavigationComposer::class);
    }

    public function register()
    {
        //
    }
}
