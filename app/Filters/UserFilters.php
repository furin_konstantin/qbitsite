<?php

namespace App\Filters;

class UserFilters extends QueryFilter
{
    
    public function sort($sort)
    {
        if ($sort) {
            return $this->builder->orderBy($sort, $this->filters()["order"]);
        }
    }
    
}