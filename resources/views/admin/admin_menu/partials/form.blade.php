    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <div class="form-group">
        {{ Form::label('active', 'Активность') }}
        {{ Form::select('active', ['1' => 'Активно', '0' => 'Не активно'], $menu_item->active ?? '', ['class' => 'form-control'])}}
    </div>

    
    <div class="form-group">
        {{ Form::label('name', 'Название') }}
        {{ Form::text('name', $menu_item->name ?? '', ['class' => 'form-control', 'required', 'placeholder' => 'Название']) }}
    </div>
    
    <div class="form-group">
        @if (!empty($menu_item))
            @if ($menu_item->getOtherPartsUrl())
                {{ Form::label('url_parts', 'Перед ссылкой путь') }}
                {{ Form::text('url_parts', $menu_item->getOtherPartsUrl() ?? '', ['class' => 'form-control', 'disabled']) }}
            @endif
        @endif
        {{ Form::label('url', 'Ссылка') }}
        {{ Form::text('url', $menu_item->url ?? '', ['class' => 'form-control', 'placeholder' => 'Ссылка', 'required']) }}
    </div>
    
    <div class="form-check">
        {{ Form::checkbox('is_absolute_link', 1, $menu_item->is_absolute_link ?? '', ['class' => 'form-check-input']) }}
        {{ Form::label('is_absolute_link', 'Абсолютная ссылка?') }}
    </div>
    
    <div class="form-group">
        {{ Form::label('sort_order', 'Сортировка') }}
        {{ Form::text('sort_order', $menu_item->sort_order ?? '', ['class' => 'form-control', 'placeholder' => 'Сортировка']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('parent_id', 'Родительский пункт меню') }}
        <select class="form-control" id="parent_id" name="parent_id">
            <option value="0">-- без родителя --</option>
            @include('admin.admin_menu.partials.menu', ['menu' => $menu])
        </select>
    </div>
    
    <div class="form-group">
        {{ Form::label('elements_model', 'Собирать вложенные пункты меню из таблицы') }}
        {{ Form::select('elements_model', $elements_model, $menu_item->elements_model ?? '', ['class' => 'form-control'])}}
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}