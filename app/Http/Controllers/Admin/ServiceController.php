<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Service;
use Illuminate\Http\Request;
use App\Filters\ServiceFilters;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ServiceFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Услуги'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'title',
                'label' => 'Наименование'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация'
            ]
        ];
        $arFilterFields = [
            [
                'name' => 'id',
                'label' => 'Идентификатор',
                'type' => 'text'
            ],
            [
                'name' => 'title',
                'label' => 'Поиск по названию',
                'type' => 'text'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация',
                'values' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано'
                ],
                'type' => 'select'
            ],
        ];
        return view('admin.services.index', [
            'breadcrumbs' => $breadcrumbs,
            'filter_fields' => $arFilterFields,
            'sort_fields' => $arSortFields,
            'services' => Service::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Услуги', 'url' => route('admin.service.index')],
            ['title' => 'Создание услуги']
        ];
        return view('admin.services.create', [
            'breadcrumbs' => $breadcrumbs,
            'service' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'slug' => ['required', 'unique:services'],
        ]);
        $data = $request->all();
        $model = new Service;
        if ($request->file('image')) {
            $data['image'] = AddFileInDB($request, $model, 'image', ['width' => 80, 'height' => 75]);
        }
        if ($request->file('image_detail_1')) {
            $data['image_detail_1'] = AddFileInDB($request, $model, 'image_detail_1', ['width' => 360, 'height' => 450]);
        }
        if ($request->file('image_detail_2')) {
            $data['image_detail_2'] = AddFileInDB($request, $model, 'image_detail_2', ['width' => 490, 'height' => 300]);
        }
        $service = Service::create($data);
        \Session::flash('success', 'Тариф успешно добавлен!');
        
        return redirect()->route('admin.service.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Сервисы', 'url' => route('admin.service.index')],
            ['title' => $service->title]
        ];
        if (url()->getRequest()->delete_file && !empty($service->{url()->getRequest()->delete_file})) {
            if (\Storage::delete(str_replace('storage', 'public', $service->{url()->getRequest()->delete_file})) && \Storage::delete(str_replace('storage', \Config::get('constants.path_thumbnail'), $service->{url()->getRequest()->delete_file}))) {
                $service->{url()->getRequest()->delete_file} = "";
                $service->save();
                \Session::flash('success', 'Изображение успешно удалено!');
            }
        }
        return view('admin.services.edit', [
            'breadcrumbs' => $breadcrumbs,
            'service' => $service,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $validator = $request->validate([
            'slug' => [
                'required', 
                \Illuminate\Validation\Rule::unique('services')->ignore($service->id)
            ],
        ]);
        $data = $request->all();
        if (!isset($data['show_in_main'])) {
            $data['show_in_main'] = false;
        }
        if ($request->file('image')) {
            if ($service->image) {
                DeleteFileFromDB($service, 'image');
            }
            $data['image'] = AddFileInDB($request, $service, 'image', ['width' => 80, 'height' => 75]);
        }

        if ($request->file('image_detail_1')) {
            if ($service->image) {
                DeleteFileFromDB($service, 'image_detail_1');
            }
            $data['image_detail_1'] = AddFileInDB($request, $service, 'image_detail_1', ['width' => 360, 'height' => 450]);
        }
        if ($request->file('image_detail_2')) {
            if ($service->image_detail_2) {
                DeleteFileFromDB($service, 'image_detail_2');
            }
            $data['image_detail_2'] = AddFileInDB($request, $service, 'image_detail_2', ['width' => 490, 'height' => 300]);
        }

        $service->update($data);
        \Session::flash('success', 'Услуга успешно обновлена!');
        
        return redirect()->route('admin.service.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        DeleteFileFromDB($service, 'image');
        DeleteFileFromDB($service, 'image_detail_1');
        DeleteFileFromDB($service, 'image_detail_2');
        
        \Session::flash('success', 'Услуга успешно удалена!');
        
        return redirect()->route('admin.service.index');
    }
}
