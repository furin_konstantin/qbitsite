    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <div class="form-group">
        {{ Form::label('name', 'Имя') }}
        {{ Form::text('name', $user->name ?? '', ['class' => 'form-control', 'placeholder' => 'Имя']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', $user->email ?? '', ['class' => 'form-control', 'placeholder' => 'Email']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('password', 'Пароль') }}
        {{ Form::password('password', ['class' => 'form-control']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('password_confirmation', 'Подтверждение пароля') }}
        {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}