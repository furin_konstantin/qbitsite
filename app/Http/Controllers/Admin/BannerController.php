<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Filters\BannerFilters;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BannerFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Баннеры'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'title',
                'label' => 'Наименование'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация'
            ]
        ];
        $arFilterFields = [
            [
                'name' => 'id',
                'label' => 'Идентификатор',
                'type' => 'text'
            ],
            [
                'name' => 'title',
                'label' => 'Поиск по названию',
                'type' => 'text'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация',
                'values' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано'
                ],
                'type' => 'select'
            ],
        ];
        return view('admin.banners.index', [
            'breadcrumbs' => $breadcrumbs,
            'filter_fields' => $arFilterFields,
            'sort_fields' => $arSortFields,
            'banners' => Banner::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Баннеры', 'url' => route('admin.banner.index')],
            ['title' => 'Создание баннера']
        ];
        return view('admin.banners.create', [
            'breadcrumbs' => $breadcrumbs,
            'banner' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $model = new Banner;
        if ($request->file('image')) {
            $data['image'] = AddFileInDB($request, $model, 'image', ['width' => 600, 'height' => 500]);
        }
        $banner = Banner::create($data);
        \Session::flash('success', 'Баннер успешно добавлен!');
        
        return redirect()->route('admin.banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Баннеры', 'url' => route('admin.banner.index')],
            ['title' => $banner->title]
        ];
        if (url()->getRequest()->delete_file && !empty($banner->{url()->getRequest()->delete_file})) {
            if (\Storage::delete(str_replace('storage', 'public', $banner->{url()->getRequest()->delete_file})) && \Storage::delete(str_replace('storage', \Config::get('constants.path_thumbnail'), $banner->{url()->getRequest()->delete_file}))) {
                $banner->{url()->getRequest()->delete_file} = "";
                $banner->save();
                \Session::flash('success', 'Изображение успешно удалено!');
            }
        }
        return view('admin.banners.edit', [
            'breadcrumbs' => $breadcrumbs,
            'banner' => $banner,
        ]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $data = $request->all();
        if ($request->file('image')) {
            if ($banner->image) {
                DeleteFileFromDB($banner, 'image');
            }
            $data['image'] = AddFileInDB($request, $banner, 'image', ['width' => 600, 'height' => 500]);
        }

        $banner->update($data);
        \Session::flash('success', 'Баннер успешно обновлен!');
        
        return redirect()->route('admin.banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        $banner->delete();
        DeleteFileFromDB($banner, 'image');
        
        \Session::flash('success', 'Баннер успешно удален!');
        
        return redirect()->route('admin.banner.index');
    }
}
