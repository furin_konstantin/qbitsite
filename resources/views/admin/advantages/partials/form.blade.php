    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        {{ Form::label('published', 'Статус') }}
        {{ Form::select('published', ['1' => 'Опубликовано', '0' => 'Не опубликовано'], $advantage->published ?? '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Название') }}
        {{ Form::text('title', $advantage->title ?? '', ['class' => 'form-control', 'placeholder' => 'Заголовок']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('sort', 'Сортировка') }}
        {{ Form::text('sort', $advantage->sort ?? '', ['class' => 'form-control', 'placeholder' => 'Сортировка']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('class', 'Класс для иконки (Flaticon)') }}
        {{ Form::text('class', $advantage->class ?? '', ['class' => 'form-control', 'placeholder' => 'Класс для иконки (Flaticon)']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('image', 'Изображение') }}
        {{ Form::file('image') }}
        @if (isset($advantage->image) && !empty($advantage->image))
            <img src="{{asset($advantage->image)}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.advantage.edit', ['advantage' => $advantage, 'delete_file' => 'image'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-group">
        {{ Form::label('text', 'Описание') }}
        {{ Form::textarea('text', $advantage->text ?? ''), ['class' => 'form-control'] }}
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}