<?php

namespace App\Filters;

class CategoryFilters extends QueryFilter
{

    public function id($id = "")
    {
        if ($id) {
            return $this->builder->where('id', $id);
        }
    }

    public function title($title = "")
    {
        if ($title) {
            return $this->builder->where('title', 'like', '%'.$title.'%');
        }
    }
    
    public function sort($sort)
    {
        if ($sort) {
            return $this->builder->orderBy($sort, $this->filters()["order"]);
        }
    }

    public function published($published = "")
    {
        if (is_numeric($published)) {
            return $this->builder->where('published', $published);
        }
    }

    // public function brands($brandIds)
    // {
        // return $this->builder->whereIn('brand_id', $this->paramToArray($brandIds));
    // }

    // public function search($keyword)
    // {
        // return $this->builder->where('title', 'like', '%'.$keyword.'%');
    // }

    // public function price($order = 'asc')
    // {
        // return $this->builder->orderBy('price', $order);
    // }
    
}