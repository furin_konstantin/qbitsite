<?php

namespace App\Filters;

class MenuFilters extends QueryFilter
{
    
    public function sort($sort)
    {
        if ($sort) {
            return $this->builder->orderBy($sort, $this->filters()["order"]);
        }
    }
    
}