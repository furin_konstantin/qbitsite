<?php

namespace App\Http\Controllers\Admin\AdminMenu;

use App\Http\Controllers\Controller;
use App\HeaderMenu;
use App\FooterMenu;
use Illuminate\Http\Request;
use App\Filters\MenuFilters;
use Illuminate\Support\Facades\Input;

class AdminMenuController extends Controller
{

    protected $menuClass = "";

    public function index($model, MenuFilters $filters)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => $menu->title],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'name',
                'label' => 'Название'
            ],
            [
                'name' => 'url',
                'label' => 'Ссылка'
            ],
            [
                'name' => 'live',
                'label' => 'Активность'
            ],
            [
                'name' => 'sort_order',
                'label' => 'Сортировка'
            ],
            [
                'name' => 'parent_id',
                'label' => 'Родитель'
            ]
        ];
        $menuItems = $menu::filter($filters)->paginate(10);
        return view('admin.admin_menu.index', [
            'breadcrumbs' => $breadcrumbs,
            'model' => $model,
            'sort_fields' => $arSortFields,
            'menu_items' => $menuItems
        ]);
    }

    public function create($model)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => $menu->title, 'url' => route('admin.menu.index', ['model' => $model])],
            ['title' => 'Создание пункта меню']
        ];
        $elements_model = $menu->getElementsModel();
        return view('admin.admin_menu.create', [
            'breadcrumbs' => $breadcrumbs,
            'model' => $model,
            'menu' => $menu::with('children')->where('parent_id', 0)->get(),
            'menu_item' => [],
            'delimiter' => '',
            'elements_model' => $elements_model
        ]);
    }

    public function store(Request $request, $model)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $menu::create($request->all());
        \Session::flash('success', 'Пункт меню успешно добавлен!');
        return redirect()->route('admin.menu.index', ['model' => $model]);
    }

    public function edit($model, $item_id)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $menu_item = $menu::where('id', $item_id)->first();
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => $menu->title, 'url' => route('admin.menu.index', ['model' => $model])],
            ['title' => 'Редактирование пункта меню ' . $menu_item->name]
        ];
        $elements_model = $menu->getElementsModel();
        return view('admin.admin_menu.edit', [
            'breadcrumbs' => $breadcrumbs,
            'model' => $model,
            'menu' => $menu::with('children')->where('parent_id', 0)->get(),
            'delimiter' => '',
            'menu_item' => $menu_item,
            'elements_model' => $elements_model
        ]);
    }

    public function update(Request $request, $model, $item_id)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $item = $menu::where('id', $item_id);
        $data = $request->except('_method', '_token');
        if (!isset($data['is_absolute_link'])) {
            $data['is_absolute_link'] = false;
        }
        $item->update($data);
        \Session::flash('success', 'Пункт меню успешно обновлен!');
        return redirect()->route('admin.menu.index', ['model' => $model]);
    }

    public function destroy($model, $item_id)
    {
        $this->setMenuClass($model);
        $menu = new $this->menuClass;
        $item = $menu::where('id', $item_id);
        $item->delete();
        \Session::flash('success', 'Пункт меню успешно удален!');
        return redirect()->route('admin.menu.index', ['model' => $model]);
    }
    
        
    public function setMenuClass($model) {
        $this->menuClass = 'App\\'.ucfirst($model).'Menu';
        if (!class_exists($this->menuClass)) {
            abort(403);
        }
    }

}
