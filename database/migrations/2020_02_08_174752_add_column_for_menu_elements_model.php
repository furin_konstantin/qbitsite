<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnForMenuElementsModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::table('header_menus', function (Blueprint $table) {
            $table->string('elements_model')->nullable();
        });
    
        Schema::table('footer_menus', function (Blueprint $table) {
            $table->string('elements_model')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
        if (Schema::hasColumn('header_menus', 'elements_model'))
        {
            Schema::table('header_menus', function(Blueprint $table) {
                $table->dropColumn(array('elements_model'));
            });
        }
    
        if (Schema::hasColumn('footer_menus', 'elements_model'))
        {
            Schema::table('footer_menus', function(Blueprint $table) {
                $table->dropColumn(array('elements_model'));
            });
        }
    }
}
