const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Mix for admin panel
mix.js('resources/admin_template/js/app.js', 'public/admin_template/js')
   .sass('resources/admin_template/sass/app.scss', 'public/admin_template/css');

// Mix for site
mix.copy('resources/site/js/respond.js', 'public/site/js/respond.js');
mix.copyDirectory('resources/site/css/images', 'public/site/css/images');
mix.scripts([
    'resources/site/js/jquery.js',
    'resources/site/js/popper.min.js',
    'resources/site/js/jquery-ui.js',
    'resources/site/js/bootstrap.min.js',
    'resources/site/js/jquery.fancybox.js',
    'resources/site/js/owl.js',
    'resources/site/js/validate.js',
    'resources/site/js/nav-tool.js',
    'resources/site/js/scrollbar.js',
    'resources/site/js/wow.js',
    'resources/site/js/appear.js',
    'resources/site/js/script.js',
    'resources/site/js/map-script.js'
], 'public/site/js/app.js');
mix.copyDirectory('resources/site/fonts', 'public/site/fonts');
mix.copyDirectory('resources/site/images', 'public/site/images');
mix.styles([
    'resources/site/css/bootstrap.css',
    'resources/site/css/style.css',
    'resources/site/css/fontawesome-all.css',
    'resources/site/css/animate.css',
    'resources/site/css/flaticon.css',
    'resources/site/css/owl.css',
    'resources/site/css/jquery-ui.css',
    'resources/site/css/custom-animate.css',
    'resources/site/css/jquery.fancybox.min.css',
    'resources/site/css/scrollbar.css',
    'resources/site/css/responsive.css'
], 'public/site/css/app.css');
