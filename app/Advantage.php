<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class Advantage extends Model
{
    protected $fillable = ['title', 'text', 'sort', 'class', 'image', 'published'];
    
    public $pathImages = "public/advantages";
    public $pathThumbsImages = "public/thumbnail/advantages";
    public $pathStorageImages = "storage/advantages";
    public $pathStorageThumbImages = "storage/thumbnail/advantages";
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
    public function getThumbImage() {
        $res = str_replace($this->pathStorageImages, $this->pathStorageThumbImages, $this->image);
        return $res;
    }
    
    public function scopeLastAdvantages($query, $count) {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
    
}
