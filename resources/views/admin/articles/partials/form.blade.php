    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        {{ Form::label('published', 'Статус') }}
        {{ Form::select('published', ['1' => 'Опубликовано', '0' => 'Не опубликовано'], $article->published ?? '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Название') }}
        {{ Form::text('title', $article->title ?? '', ['class' => 'form-control', 'required', 'placeholder' => 'Заголовок статьи']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('slug', 'Символьный код') }}
        {{ Form::text('slug', $article->slug ?? '', ['class' => 'form-control', 'placeholder' => 'Символьный код']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('image', 'Изображение') }}
        {{ Form::file('image') }}
        @if (isset($article->image) && !empty($article->image))
            <img src="{{asset($article->image)}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.article.edit', ['article' => $article, 'delete_file' => 'image'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-check">
        {{ Form::checkbox('image_show', 1, $article->image_show ?? '', ['class' => 'form-check-input']) }}
        {{ Form::label('image_show', 'Показывать изображение') }}
    </div>
    
    <div class="form-group">
        {{ Form::label('categories', 'Родительская категория') }}
        <select class="form-control" id="categories" name="categories[]" multiple="">
            @include('admin.articles.partials.articles', ['categories' => $categories])
        </select>
    </div>
    
    <div class="form-group">
        {{ Form::label('description', 'Полное описание') }}
        {{ Form::textarea('description', $article->description ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('description_short', 'Краткое описание') }}
        {{ Form::textarea('description_short', $article->description_short ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_title', 'Мета-заголовок') }}
        {{ Form::text('meta_title', $article->meta_title ?? '', ['class' => 'form-control', 'placeholder' => 'Мета-заголовок']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_description', 'Мета-описание') }}
        {{ Form::text('meta_description', $article->meta_description ?? '', ['class' => 'form-control', 'placeholder' => 'Мета-описание']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_keyword', 'Ключевые слова') }}
        {{ Form::text('meta_keyword', $article->meta_keyword ?? '', ['class' => 'form-control', 'placeholder' => 'Ключевые слова']) }}
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}