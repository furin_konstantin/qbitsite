<?php

    if (!function_exists('AddFileInDB')) {
        function AddFileInDB($request, $model, $name, $sizes = ['width' => 200, 'height' => 200]) {
            $res = "";
            $file = $request->file($name);
            $filename = Str::random(20) . '.' . $file->getClientOriginalExtension() ? :  'png';
            $pathImageForDB = $request->file($name)->storeAs($model->pathImages, $filename);
            $request->file($name)->storeAs($model->pathThumbsImages, $filename);
            $thumbnailpath = public_path(str_replace('public', 'storage', $model->pathThumbsImages) . "/" . $filename);
            $img = Image::make($thumbnailpath)->resize($sizes['width'], $sizes['height'], function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);
            $res = str_replace('public', '/storage', $pathImageForDB);
            return $res;
        }
    }
    
    if (!function_exists('DeleteFileFromDB')) {
        function DeleteFileFromDB($model, $name) {
            Storage::delete(str_replace('storage', 'public', $model->{$name}));
            Storage::delete(Storage::delete(str_replace('storage', Config::get('constants.path_thumbnail'), $model->{$name})));
        }
    }
    
    if (!function_exists('IsActiveItemMenu')) {
        function IsActiveItemMenu($link) {
            $res = false;
            if (url()->current() == $link) {
                $res = true;
            }
            return $res;
        }
    }