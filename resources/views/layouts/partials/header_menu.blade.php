@if($header_menu_items)
    <div class="nav-outer clearfix">
        <!--Mobile Navigation Toggler For Mobile--><div class="mobile-nav-toggler default-mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>
        <nav class="main-menu navbar-expand-md navbar-light">
            <div class="navbar-header">
                <!-- Togg le Button -->
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon flaticon-menu"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                <ul class="navigation clearfix">
                    @php
                        function buildHeaderMenu($items)
                        {
                            foreach ($items as $item) {
                                if (isset($item->children)) {
                                @endphp
                                    <li class="{{$item->isActiveItemMenu() ? 'current' : ''}} {{(count($item->children) > 0 || ($item->elements_model && count($item->modelChildren) > 0)) ? 'dropdown' : ''}}"><a href="{{$item->getFullUrl()}}">{{$item->name}}</a>
                                        <ul>
                                            @if ($item->elements_model)
                                                @php
                                                    if (count($item->modelChildren) > 0) {
                                                        foreach($item->modelChildren as $modelChild) {
                                                            @endphp
                                                                <li class="{{IsActiveItemMenu($modelChild->link) ? 'current' : ''}}"><a href="{{$modelChild->link}}">{{$modelChild->title}}</a></li>
                                                            @php
                                                        }
                                                    }
                                                @endphp
                                            @endif
                                            @php buildHeaderMenu($item->children) @endphp
                                        </ul>
                                @php
                                    } else {
                                @endphp
                                    <li class="{{$item->isActiveItemMenu() ? 'current' : ''}} {{(count($item->children) > 0) ? 'dropdown' : ''}}"><a href="{{$item->getFullUrl()}}">{{$item->name}}</a>
                                @php
                                    }
                                @endphp
                                    </li>
                                @php
                            }
                        }

                        buildHeaderMenu($header_menu_items)
                    @endphp
                </ul>
            </div>
        </nav>
        <!-- Main Menu End-->
    </div>
@endif