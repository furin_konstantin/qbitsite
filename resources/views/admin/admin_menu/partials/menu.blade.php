@foreach ($menu as $menu_item_list)
    <option value="{{$menu_item_list->id ?? ''}}"
        @isset($menu_item->id)
        
            @if ($menu_item->parent_id == $menu_item_list->id)
                selected=""
            @endif
            
            @if ($menu_item->id == $menu_item_list->id)
                hidden=""
            @endif
        
        @endisset
    >
        {!! $delimiter ?? '' !!}{{$menu_item_list->name ?? ''}}
    </option>
    
    @if (count($menu_item_list->children) > 0)
    
        @include('admin.admin_menu.partials.menu', [
            'menu' => $menu_item_list->children,
            'delimiter' => ' - ' . $delimiter
        ])
    
    @endif
@endforeach