@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список пунктов меню @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => ['admin.menu.store', 'model' => $model], 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.admin_menu.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection