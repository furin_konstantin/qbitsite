@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список категорий @endslot
        @endcomponent
        
        <hr>

        {{ Form::open(['route' => 'admin.category.store', 'class' => 'form-horizontal', 'method' => 'post']) }}
            @include('admin.categories.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection