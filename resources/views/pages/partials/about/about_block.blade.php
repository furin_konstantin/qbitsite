<div class="title-column col-lg-6 col-md-12 col-sm-12">
    <div class="inner-column">
        <!-- Sec Title -->
        <div class="sec-title">
            <div class="sec-icons">
                <span class="icon-one"></span>
                <span class="icon-two"></span>
                <span class="icon-three"></span>
            </div>
            <h2>Last year we proved that <br> we’re the best SEO house <br> in America!</h2>
        </div>
        <div class="text">Dolor sit amet consectetur elit sed do eiusmod tempor incididunt labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco labo ris nisi ut aliquip ex ea commodo consequat.duis aute irure dolor in reprehenderit.</div>
    </div>
</div>