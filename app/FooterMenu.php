<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BaseMenuTrait;

class FooterMenu extends Model
{
    use BaseMenuTrait;
    
    public $title = "Нижнее меню";
    
    public $fillable = [
        'parent_id',
        'name',
        'url',
        'active',
        'sort_order',
        'elements_model'
    ];

    public $timestamps = false;

}