@if ($sort_fields)
    @php
        $order = (url()->getRequest()->order == "asc") ? "desc" : "asc";
    @endphp
    @foreach ($sort_fields as $sort_field)
        <th>{{$sort_field['label']}} 
        @php
            if (url()->getRequest()->query()) {
                $sort_url = url()->full().'&sort='.$sort_field['name'].'&order='.$order;
            } else {
                $sort_url = url()->current().'?sort='.$sort_field['name'].'&order='.$order;
            }
            $sort = $sort_field['name'];
        @endphp
        @if (!empty(url()->getRequest()->order && $sort == url()->getRequest()->sort))
            @if ($sort == url()->getRequest()->sort && url()->getRequest()->order == "asc")
                <i class="fas fa-sort-amount-down js-sort" data-sort_url="{{ $sort_url }}"></i>
            @endif
            @if ($sort == url()->getRequest()->sort && url()->getRequest()->order == "desc")
                <i class="fas fa-sort-amount-up js-sort" data-sort_url="{{ $sort_url }}"></i>
            @endif
        @else
            <i class="fas fa-frog js-sort" data-sort_url="{{ $sort_url }}"></i>
        @endif
        </th>
    @endforeach
@endif

