    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        {{ Form::label('published', 'Статус') }}
        {{ Form::select('published', ['1' => 'Опубликовано', '0' => 'Не опубликовано'], $price->published ?? '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Название') }}
        {{ Form::text('title', $price->title ?? '', ['class' => 'form-control', 'required', 'placeholder' => 'Название']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('slug', 'Символьный код') }}
        {{ Form::text('slug', $price->slug ?? '', ['class' => 'form-control', 'placeholder' => 'Символьный код']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('sort', 'Сортировка') }}
        {{ Form::text('sort', $price->sort ?? '', ['class' => 'form-control', 'placeholder' => 'Сортировка']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('price', 'Цена') }}
        {{ Form::text('price', $price->price ?? '', ['class' => 'form-control', 'placeholder' => 'Цена']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('image', 'Анонсовое изображение') }}
        {{ Form::file('image') }}
        @if (isset($price->image) && !empty($price->image))
            <img src="{{asset($price->getThumbImage('image'))}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.price.edit', ['price' => $price, 'delete_file' => 'image'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-group">
        {{ Form::label('image_detail_1', 'Изображение 1') }}
        {{ Form::file('image_detail_1') }}
        @if (isset($price->image_detail_1) && !empty($price->image_detail_1))
            <img src="{{asset($price->getThumbImage('image_detail_1'))}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.price.edit', ['price' => $price, 'delete_file' => 'image_detail_1'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-group">
        {{ Form::label('image_detail_2', 'Изображение 2') }}
        {{ Form::file('image_detail_2') }}
        @if (isset($price->image_detail_2) && !empty($price->image_detail_2))
            <img src="{{asset($price->getThumbImage('image_detail_2'))}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.price.edit', ['price' => $price, 'delete_file' => 'image_detail_2'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-check">
        {{ Form::checkbox('show_in_main', 1, $price->show_in_main ?? '', ['class' => 'form-check-input']) }}
        {{ Form::label('show_in_main', 'Показывать на главной?') }}
    </div>
    
    <div class="form-group">
        {{ Form::label('description', 'Полное описание') }}
        {{ Form::textarea('description', $price->description ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('description_short', 'Краткое описание') }}
        {{ Form::textarea('description_short', $price->description_short ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('additional_text', 'Дополнительное описание') }}
        {{ Form::textarea('additional_text', $price->additional_text ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_title', 'Мета-заголовок') }}
        {{ Form::text('meta_title', $price->meta_title ?? '', ['class' => 'form-control', 'placeholder' => 'Мета-заголовок']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_description', 'Мета-описание') }}
        {{ Form::text('meta_description', $price->meta_description ?? '', ['class' => 'form-control', 'placeholder' => 'Мета-описание']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('meta_keyword', 'Ключевые слова') }}
        {{ Form::text('meta_keyword', $price->meta_keyword ?? '', ['class' => 'form-control', 'placeholder' => 'Ключевые слова']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('h1', 'Заголовок H1') }}
        {{ Form::text('h1', $price->h1 ?? '', ['class' => 'form-control', 'placeholder' => 'Заголовок H1']) }}
    </div>
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}