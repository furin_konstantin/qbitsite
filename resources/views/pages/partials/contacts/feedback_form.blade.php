<!-- Form Column -->
<div class="form-column col-lg-8 col-md-12 col-sm-12">
    <div class="inner-column">
        <div class="sec-title">
            <h2>Форма обратной связи</h2>
        </div>
        
        <!-- Contact Form -->
        <div class="contact-form">
            
            <!--Contact Form-->
            {{ Form::open(['route' => 'contacts.feedback', 'method' => 'post', 'enctype' => 'multipart/form-data', 'id' => 'contact-form']) }}
                <div class="row clearfix">
                
                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        {{ Form::text('username', '', ['required', 'placeholder' => 'Ваше имя']) }}
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        {{ Form::email('email', '', ['required', 'placeholder' => 'Ваш Email']) }}
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                         {{ Form::text('phone', '', ['required', 'placeholder' => 'Ваш номер телефона']) }}
                    </div>
                    
                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                        {{ Form::text('subject', '', ['required', 'placeholder' => 'Тема обращения']) }}
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                        {{ Form::textarea('message', '', ['required', 'placeholder' => 'Ваше сообщение']) }}
                    </div>
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                        <button class="theme-btn btn-style-four" type="submit" name="submit-form"><span class="txt">Отправить</span></button>
                    </div>
                    
                </div>
            </form>
            
            <!--End Contact Form -->
        </div>
        
    </div>
</div>