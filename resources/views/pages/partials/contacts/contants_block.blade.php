<!-- Contact Info Section -->
<section class="contact-info-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!-- Info Block -->
            <div class="contact-info-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon">
                            {{-- {{ Html::image(asset(Config::get('constants.path_main_public_template').'/images/icons/icon-4.png'), 'Адрес', ['title' => 'Адрес']) }} --}}
                            <img src="{{asset(Config::get('constants.path_main_public_template').'/images/icons/icon-4.png')}}" alt="Адрес" title="Адрес">
                        </span>
                    </div>
                    <h3>Наш адрес:</h3>
                    <div class="text">Уфа <br> Степана Кувыкина 24/1</div>
                </div>
            </div>
            <!-- End Info Block -->
            
            <!-- Info Block -->
            <div class="contact-info-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon">     
                            <img src="{{asset(Config::get('constants.path_main_public_template').'/images/icons/icon-5.png')}}" alt="Телефон" title="Телефон">
                        </span>
                    </div>
                    <h3>Позвонить:</h3>
                    <ul>
                        <li><a href="tel:(+48)564-334-21-22-34">(+7) (347) 216-32-60</a></li>
                        <li><a href="tel:(+48)564-334-21-25">(+7) (347) 216-32-61</a></li>
                    </ul>
                </div>
            </div>
            <!-- End Info Block -->
            
            <!-- Info Block -->
            <div class="contact-info-block col-lg-4 col-md-6 col-sm-12">
                <div class="inner-box">
                    <div class="icon-box">
                        <span class="icon"> <img src="{{asset(Config::get('constants.path_main_public_template').'/images/icons/icon-6.png')}}" alt="Почта" title="Почта">
                        </span>
                    </div>
                    <h3>Написать нам:</h3>
                    <ul>
                        <li>{{Html::mailto('info@q-bit.site', 'info@q-bit.site')}}</li>
                        <li>{{Html::mailto('iinfo@q-bit.site', 'info@q-bit.site')}}</li>
                    </ul>
                </div>
            </div>
            <!-- End Info Block -->
            
        </div>
    </div>
</section>