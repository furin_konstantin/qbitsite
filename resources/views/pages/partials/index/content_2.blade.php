<!-- Image Column -->
<div class="image-column col-lg-6 col-md-12 col-sm-12">
    <div class="inner-column wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
        <div class="image">
            <img src="{{ asset(Config::get('constants.path_main_public_template').'/images/resource/laptop.png')}}" alt="" />
        </div>
    </div>
</div>