@foreach ($categories as $category)
    <option value="{{$category->id ?? ''}}"
        @isset($article->id)
            @foreach ($article->categories as $category_article)
                @if ($category_article->id == $category->id)
                    selected="selected"
                @endif
            @endforeach
        @endisset
    >
        {!! $delimiter ?? '' !!}{{$category->title ?? ''}}
    </option>
    
    @if (count($category->children) > 0)
    
        @include('admin.articles.partials.articles', [
            'categories' => $category->children,
            'delimiter' => ' - ' . $delimiter
        ])
    
    @endif
@endforeach