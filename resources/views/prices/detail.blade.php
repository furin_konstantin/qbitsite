@extends('layouts.inner')

@section('title', $price['meta_title'])
@section('meta_keywords', $price['meta_keyword'])
@section('meta_description', $price['meta_description'])
@section('h1', $price['h1'])

@section('content')
    @parent
    <section class="seo-section-six">
		<div class="auto-container">
			<div class="row clearfix">
				<!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">
						<h2>{{$price['title']}}</h2>
						<div class="text">
							{!! $price['description'] !!}
						</div>
					</div>
				</div>
				<!-- Images Column -->
                @if ($price->image_detail_1 && $price->image_detail_2)
                    <div class="image-column col-lg-6 col-md-12 col-sm-12">
                        <div class="inner-column clearfix">
                            <div class="image image-one wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <img src="{{asset($price->getThumbImage('image_detail_1'))}}" alt="{{$price['title']}}" />
                            </div>
                            <div class="image image-two wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <img src="{{asset($price->getThumbImage('image_detail_2'))}}" alt="{{$price['title']}}" />
                            </div>
                        </div>
                    </div>
                @endif
			</div>
		</div>
	</section>
    <section class="services-page-section style-two">
		<div class="auto-container">
			<div class="sec-title centered">
				<h2>Наши услуги</h2>
			</div>
			
			<div class="row clearfix">
				
				@component('widgets.services', ['services' => $services]) @endcomponent

			</div>
		</div>
	</section>    
@endsection