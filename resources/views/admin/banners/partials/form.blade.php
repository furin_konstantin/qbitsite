    {{-- $erros->any() --}}
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="form-group">
        {{ Form::label('published', 'Статус') }}
        {{ Form::select('published', ['1' => 'Опубликовано', '0' => 'Не опубликовано'], $banner->published ?? '', ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        {{ Form::label('title', 'Название') }}
        {{ Form::text('title', $banner->title ?? '', ['class' => 'form-control', 'placeholder' => 'Заголовок']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('sort', 'Сортировка') }}
        {{ Form::text('sort', $banner->sort ?? '', ['class' => 'form-control', 'placeholder' => 'Сортировка']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('image', 'Изображение') }}
        {{ Form::file('image') }}
        @if (isset($banner->image) && !empty($banner->image))
            <img src="{{asset($banner->image)}}" width="100" height="100">
            <a onclick="if(confirm('Удалить?')){ return true; } else { return false; }" href="{{route('admin.banner.edit', ['banner' => $banner, 'delete_file' => 'image'])}}">Удалить файл</a>
        @endif
    </div>
    
    <div class="form-group">
        {{ Form::label('text', 'Описание') }}
        {{ Form::textarea('text', $banner->text ?? ''), ['class' => 'form-control'] }}
    </div>
    
    <div class="form-group">
        {{ Form::label('button_title', 'Подпись кнопки') }}
        {{ Form::text('button_title', $banner->button_title ?? '', ['class' => 'form-control', 'placeholder' => 'Подпись кнопки']) }}
    </div>
    
    <div class="form-group">
        {{ Form::label('link', 'Ссылка') }}
        {{ Form::text('link', $banner->link ?? '', ['class' => 'form-control', 'placeholder' => 'Ссылка']) }}
    </div>
    
    
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}