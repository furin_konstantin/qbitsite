<?php

namespace App\Http\Controllers\Admin;

use App\Advantage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Filters\AdvantageFilters;

class AdvantageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AdvantageFilters $filters)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Преимущества'],
        ];
        $arSortFields = [
            [
                'name' => 'id',
                'label' => '#'
            ],
            [
                'name' => 'title',
                'label' => 'Наименование'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация'
            ]
        ];
        $arFilterFields = [
            [
                'name' => 'id',
                'label' => 'Идентификатор',
                'type' => 'text'
            ],
            [
                'name' => 'title',
                'label' => 'Поиск по названию',
                'type' => 'text'
            ],
            [
                'name' => 'published',
                'label' => 'Публикация',
                'values' => [
                    0 => 'Не опубликовано',
                    1 => 'Опубликовано'
                ],
                'type' => 'select'
            ],
        ];
        return view('admin.advantages.index', [
            'breadcrumbs' => $breadcrumbs,
            'filter_fields' => $arFilterFields,
            'sort_fields' => $arSortFields,
            'advantages' => Advantage::filter($filters)->orderBy('created_at', 'desc')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Преимущества', 'url' => route('admin.advantage.index')],
            ['title' => 'Создание преимущества']
        ];
        return view('admin.advantages.create', [
            'breadcrumbs' => $breadcrumbs,
            'advantage' => [],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $model = new Advantage;
        if ($request->file('image')) {
            $data['image'] = AddFileInDB($request, $model, 'image', ['width' => 80, 'height' => 75]);
        }
        $advantage = Advantage::create($data);
        \Session::flash('success', 'Преимущество успешно добавлено!');
        
        return redirect()->route('admin.advantage.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advantage  $advantage
     * @return \Illuminate\Http\Response
     */
    public function show(Advantage $advantage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advantage  $advantage
     * @return \Illuminate\Http\Response
     */
    public function edit(Advantage $advantage)
    {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('admin.index')],
            ['title' => 'Преимущества', 'url' => route('admin.advantage.index')],
            ['title' => $advantage->title]
        ];
        if (url()->getRequest()->delete_file && !empty($advantage->{url()->getRequest()->delete_file})) {
            if (\Storage::delete(str_replace('storage', 'public', $advantage->{url()->getRequest()->delete_file})) && \Storage::delete(str_replace('storage', \Config::get('constants.path_thumbnail'), $advantage->{url()->getRequest()->delete_file}))) {
                $advantage->{url()->getRequest()->delete_file} = "";
                $advantage->save();
                \Session::flash('success', 'Изображение успешно удалено!');
            }
        }
        return view('admin.advantages.edit', [
            'breadcrumbs' => $breadcrumbs,
            'advantage' => $advantage,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advantage  $advantage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advantage $advantage)
    {
        $data = $request->all();
        if ($request->file('image')) {
            if ($advantage->image) {
                DeleteFileFromDB($advantage, 'image');
            }
            $data['image'] = AddFileInDB($request, $advantage, 'image', ['width' => 80, 'height' => 75]);
        }

        $advantage->update($data);
        \Session::flash('success', 'Преимущество успешно обновлено!');
        
        return redirect()->route('admin.advantage.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advantage  $advantage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advantage $advantage)
    {
        $advantage->delete();
        DeleteFileFromDB($advantage, 'image');
        
        \Session::flash('success', 'Преимущество успешно удалено!');
        
        return redirect()->route('admin.advantage.index');
    }
}
