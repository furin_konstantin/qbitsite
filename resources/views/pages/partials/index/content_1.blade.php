<!-- Content Column -->
<div class="content-column col-lg-6 col-md-12 col-sm-12">
    <div class="inner-column">
        <!-- Sec Title -->
        <div class="sec-title">
            <div class="sec-icons">
                <span class="icon-one"></span>
                <span class="icon-two"></span>
                <span class="icon-three"></span>
            </div>
            <h2>Last year we proved that <br> we’re the best SEO house <br> in America!</h2>
        </div>
        <div class="text">Dolor sit amet consectetur elit sed do eiusmod tempor incid idunt labore et dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Ex cepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem.</div>
        <a href="#" class="theme-btn btn-style-two"><span class="txt">Purchase Now</span></a>
    </div>
</div>