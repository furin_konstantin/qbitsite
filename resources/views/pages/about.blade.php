@extends('layouts.inner')

@section('title', 'О нас')
@section('meta_keywords', 'О нас')
@section('meta_description', 'О нас')
@section('h1', 'О нас')

@section('content')
    @parent
    <section class="seo-section-two">
		<div class="auto-container">
			<div class="row clearfix">
				
				@include('pages.partials.about.about_block')
                @include('pages.partials.about.about_block_2')
				
			</div>
		</div>
	</section>
    <section class="services-section-five">
		<div class="image-layer" style="background-image: url({{ asset(Config::get('constants.path_main_public_template').'/images/background/2.jpg')}})"></div>
		<div class="auto-container">
			<!-- Sec Title -->
			<div class="sec-title light centered">
				<h2>Наши услуги</h2>
			</div>
			<div class="row clearfix">
				
				@component('widgets.services', ['services' => $services]) @endcomponent
				
			</div>
		</div>
	</section>
    @if (count($advantages) > 0)
        <section class="choose-section-two">
            <div class="map-pattern-layer" style="background-image: url({{ asset(Config::get('constants.path_main_public_template').'/images/background/pattern-9.png')}})"></div>
            <div class="auto-container">
                
                <!-- Sec Title -->
                <div class="sec-title">
                    <h2>Наши преимущества</h2>
                </div>
                <div class="row clearfix">
                    
                    <!-- Featured Column -->
                    <div class="featured-column col-lg-5 col-md-12 col-sm-12">
                        <div class="inner-column">
                            
                            <div class="blocks-outer">
                                
                                @foreach ($advantages as $advantage)
                                    <div class="featured-block">
                                        <div class="inner-box">
                                            <div class="content">
                                                @if ($advantage->image)
                                                    <div class="icon"><img src="{{asset($advantage->getThumbImage('image'))}}" alt="{{$advantage->title}}" /></div>
                                                @endif
                                                <h3>{{$advantage->title}}</h3>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                
                            </div>
                            
                        </div>
                    </div>
                    
                    <!-- Image Column -->
                    <div class="images-column col-lg-7 col-md-12 col-sm-12">
                        <div class="inner-column clearfix">
                            
                            <div class="image image-one wow fadeInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <img src="{{ asset(Config::get('constants.path_main_public_template').'/images/resource/seo-5.jpg')}}" alt="" />
                            </div>
                            <div class="image image-two wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <img src="{{ asset(Config::get('constants.path_main_public_template').'/images/resource/seo-6.jpg')}}" alt="" />
                            </div>
                            
                        </div>
                    </div>
                
                </div>
                
            </div>
        </section>
    @endif
    
    @include('pages.partials.about.digits')
    
@endsection