<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class Banner extends Model
{

    protected $fillable = ['title', 'text', 'sort', 'image', 'button_title', 'link', 'published'];
    
    public $pathImages = "public/banners";
    public $pathThumbsImages = "public/thumbnail/banners";
    public $pathStorageImages = "storage/banners";
    public $pathStorageThumbImages = "storage/thumbnail/banners";
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
    public function getThumbImage() {
        $res = str_replace($this->pathStorageImages, $this->pathStorageThumbImages, $this->image);
        return $res;
    }
    
    public function scopeLastBanners($query, $count) {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
    
}
