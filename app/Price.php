<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Filters\QueryFilter;

class Price extends Model
{

    protected $fillable = ['title', 'slug', 'description_short', 'description', 'image', 'meta_title', 'meta_description', 'meta_keyword', 'published', 'additional_text', 'image_detail_1', 'image_detail_2', 'show_in_main', 'price', 'h1', 'sort'];
    
    public $pathImages = "public/prices";
    public $pathThumbsImages = "public/thumbnail/prices";
    public $pathStorageImages = "storage/prices";
    public $pathStorageThumbImages = "storage/thumbnail/prices";
    
    // Mutators
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug($value);
    }
    
    public function scopeLastPrices($query, $count) {
        return $query->orderBy('created_at', 'desc')->take($count)->get();
    }
    
    public function getThumbImage($nameField) {
        $res = str_replace($this->pathStorageImages, $this->pathStorageThumbImages, $this->{$nameField});
        return $res;
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
}
