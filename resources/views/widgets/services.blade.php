@foreach($services as $service)
    <div class="service-block col-lg-4 col-md-6 col-sm-12">
        <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
            <div class="icon-outer">
                <div class="icon-box">
                    <span class="icon"><img src="{{asset($service->getThumbImage('image'))}}" alt="{{$service->title}}" /></span>
                </div>
            </div>
            <h3>{{$service->title}}</h3>
            <div class="text">{!! $service->description_short !!}</div>
            
            <!--Overlay Box-->
            <div class="overlay-box">
                <div class="overlay-inner">
                    <div class="content">
                        <h4><a href="{{route('service.detail', $service->slug)}}">{{$service->title}}</a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach