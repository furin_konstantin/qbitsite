@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список пользователей @endslot
        @endcomponent
        
        <hr>
        
        <a href="{{route('admin.user_managment.user.create')}}" class="btn btn-primary pull-right"><i class="fa fa-plus-square-o"></i> Создать пользователя</a>
        @if (Session::has('error'))
            <div class="alert alert-danger">
                {{ Session::get('error') }}
            </div>
        @endif
        @if (Session::has('success'))
            <div class="alert alert-success" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
        <table class="table table-striped">
            <thead>
                @include('admin.sort')
                <th class="text-right">Действие</th>
            </thead>
            <tbody>
                @forelse ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->strRole()}}</td>
                        <td class="text-right">
                            <form onsubmit="if(confirm('Удалить?')){ return true; } else { return false; }" action="{{route('admin.user_managment.user.destroy', $user)}}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <a href="{{route('admin.user_managment.user.edit', ['user' => $user])}}"><i class="fa fa-edit"></i></a>
                                <button type="submit" class="btn"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="{{count($sort_fields)+1}}" class="text-center"><h2>Данные отсутствуют</h2></td>
                    </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="{{count($sort_fields)+1}}">
                        <ul class="pagination pull-right">
                            {{$users->links()}}
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
@endsection