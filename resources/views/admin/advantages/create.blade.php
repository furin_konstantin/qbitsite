@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список преимуществ @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.advantage.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.advantages.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection