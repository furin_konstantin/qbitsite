<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use App\Filters\QueryFilter;

class Category extends Model
{

    protected $fillable = ['title', 'slug', 'parent_id', 'published', 'created_by', 'modified_by'];
    
    // Mutators
    public function setSlugAttribute($value) {
        $this->attributes['slug'] = Str::slug($value);
    }
    
    public function children() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }
    
    public function articles() {
        return $this->morphedByMany('App\Article', 'categoryable');
    }
    
    public function scopeLastCategories($query, $count) {
        return $query->orderBy('created_by', 'desc')->take($count)->get();
    }
    
    public function scopeFilter(Builder $builder, QueryFilter $filters) {
        return $filters->apply($builder);
    }
    
}
