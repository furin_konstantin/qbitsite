@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">

        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Список тарифов @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => 'admin.price.store', 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            @include('admin.prices.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection