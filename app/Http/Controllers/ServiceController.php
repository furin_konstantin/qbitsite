<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class ServiceController extends Controller
{
    public function detail($slug) {
        $service = Service::where('slug', $slug)->first();
        if (!$service) {
            abort(404);
        }
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Тарифы', 'url' => route('service.index')],
            ['title' => $service['title']],
        ];
        $services = Service::where('id', '!=', $service['id'])->indexServices(6);
        return view('services.detail', [
            'breadcrumbs' => $breadcrumbs,
            'service' => $service,
            'services' => $services
        ]);
    }
    
    public function index() {
        $breadcrumbs = [
            ['title' => 'Главная', 'url' => route('index')],
            ['title' => 'Услуги'],
        ];
        $services = Service::where('published', 1)->orderBy('created_at', 'desc')->get();
        return view('services.index', [
            'breadcrumbs' => $breadcrumbs,
            'services' => $services
        ]);
    }
}
