@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumb', ['breadcrumbs' => $breadcrumbs])
            @slot('title') Редактирование категории @endslot
        @endcomponent
        
        <hr>
        
        {{ Form::open(['route' => ['admin.category.update', $category], 'class' => 'form-horizontal', 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            @include('admin.categories.partials.form')
        {{ Form::close() }}
        
    </div>
@endsection